# /bin/bash

rm -rf out/
rm -rf device/blackwalnut/waffle_nano/sdk_liteos/.sconsign.dblite
rm -rf device/blackwalnut/waffle_nano/sdk_liteos/build/build_tmp/
rm -rf device/blackwalnut/waffle_nano/sdk_liteos/build/scripts/__pycache__/
rm -rf device/blackwalnut/waffle_nano/sdk_liteos/output/
rm -rf device/blackwalnut/waffle_nano/sdk_liteos/third_party/u-boot-v2019.07/u-boot-v2019.07/
rm -rf device/blackwalnut/waffle_nano/sdk_liteos/tools/nvtool/__pycache__/
rm -rf device/blackwalnut/waffle_nano/sdk_liteos/tools/nvtool/out_nv_bin/
rm -rf test/xdevice/build/
rm -rf test/xdevice/src/xdevice.egg-info/
rm -rf test/xts/tools/lite/build/__pycache__/
rm -rf test/xdevice/dist/
rm -rf device/blackwalnut/waffle_nano/sdk_liteos/ohos/libs
rm -rf device/blackwalnut/waffle_nano/sdk_liteos/output
rm -rf device/blackwalnut/waffle_nano/sdk_liteos/.sconsign.dblite