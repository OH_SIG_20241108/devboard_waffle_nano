/*
 * Copyright (c) 2020 BlackWalnut Labs., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "utils_efuse.h"
#include <securec.h>
#include "hal_efuse.h"
#include "ohos_errno.h"
#include "ohos_types.h"

#include <stdbool.h>

int UtilsEfuseCustomerRsvd0Read(unsigned char* read_data) {
    return HalEfuseCustomerRsvd0Read(read_data);
}